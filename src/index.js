const fs = require('fs');

const loadJS = require('./loaders/js-loader');
const loadJSON = require('./loaders/json-loader');
const loadTSX = require('./loaders/tsx-loader');
const loadYAML = require('./loaders/yaml-loader');

/**
 * @param {string | {content: string, type: 'js' | 'json' | 'tsx' | 'yml'}} fileName or content
 * @param {*} [args]
 * @return {object}
 */
module.exports = function load(fileName, args) {
	const options = {};

	if (typeof fileName !== 'object') {
		options.content = fs.readFileSync(fileName, 'utf-8');
		options.type = fileName.split('.');
		options.type = options.type[options.type.length - 1];
	} else {
		options.content = fileName.content;
		options.type = fileName.type;
	}

	switch (options.type) {
		case 'js': return loadJS(options.content, args);
		case 'json': return loadJSON(options.content);
		case 'tsx': return loadTSX(options.content, args);
		case 'yml': return loadYAML(options.content);

		default: return void 0;
	}
};
