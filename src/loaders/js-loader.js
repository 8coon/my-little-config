const log = require('debug')('my-little-config:parse:js');

/**
 * Parse JS
 * @param {string} content
 * @param {*} args
 * @return {object | null}
 */
module.exports = (content, args) => {
	try {
		const module = {
			exports: {}
		};

		(new Function('module', content))(module);

		if (typeof module.exports === 'function') {
			return module.exports(args);
		}

		return module.exports;
	} catch (error) {
		log('error:', error);
		return null;
	}
};
