const log = require('debug')('my-little-config:parse:json');

/**
 * Parse JSON
 * @param {string} content
 * @return {object | null}
 */
module.exports = content => {
	try {
		return JSON.parse(content);
	} catch (error) {
		log('error:', error);
		return null;
	}
};
