const ts = require('typescript');
const log = require('debug')('my-little-config:parse:tsx');

const jsxFactoryName = '___my_little_config_jsx_factory___';

function JsxFactory(name, props, ...children) {
	return {
		name,
		props,
		children
	};
}

function transform(src) {
	if (typeof src === 'string') {
		return src;
	}

	if (src.props && src.props.type === 'array') {
		return src.children
			.map(child => transform(child));
	}

	const result = {};

	if (src.props) {
		Object
			.keys(src.props)
			.forEach(key => result[key] = src.props[key]);
	}

	if (src.children) {
		src.children
			.forEach(child => {
				if (!child) {
					return;
				}

				if (typeof child === 'string') {
					result['$'] = child;
					return;
				}

				result[child.name] = transform(child);
			});
	}

	return result;
}

/**
 * Parse TSX
 * @param {string} content
 * @param {*} args
 * @return {object | null}
 */
module.exports = (content, args) => {
	try {
		const transformed = ts.transpileModule(content, {
			compilerOptions: {
				jsx: ts.JsxEmit.React,
				module: ts.ModuleKind.None,
				jsxFactory: jsxFactoryName,
			}
		});

		const exports = {};
		(new Function(`exports, ${jsxFactoryName}`, transformed.outputText))(exports, JsxFactory);

		let template = exports.default;

		if (typeof template === 'function') {
			template = template(args);
		}

		return transform(template);
	} catch (error) {
		global && global.process ? log('error:', error) : (() => {
			throw error;
		})();

		return null;
	}
};
