const yaml = require('js-yaml');
const log = require('debug')('my-little-config:parse:yaml');

/**
 * Parse YAML
 * @param {string} content
 * @return {object | null}
 */
module.exports = content => {
	try {
		return yaml.safeLoad(content, {
			onWarning: warning => log('warning:', warning),
			schema: yaml.DEFAULT_SAFE_SCHEMA,
			json: false
		});
	} catch (error) {
		log('error:', error);
		return null;
	}
};
