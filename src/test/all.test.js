const assert = require('assert');

const myLittleConfig = require('../index');

describe('JS', () => {
	it('Should work', () => {
		const data = myLittleConfig(__dirname + '/data/1.js', {name: 'lol'});

		assert.ok(data);
		assert.equal(data.name, 'lol');
		assert.equal(data.lol, 1);
		assert.equal(data.kek, 2);
		assert.equal(data.cheBuRek, 3);
	});
});

describe('JSON', () => {
	it('Should work', () => {
		const data = myLittleConfig(__dirname + '/data/2.json');

		assert.ok(data);
		assert.equal(data.lol, 1);
		assert.equal(data.kek, 2);
		assert.equal(data.cheBuRek, 3);
	});
});

describe('TSX', () => {
	it('Should work', () => {
		const data = myLittleConfig(__dirname + '/data/3.tsx', {name: 'lol'});

		assert.ok(data);
		assert.equal(data.name, 'lol');
		assert.ok(data.panels);
		assert.equal(data.panels.length, 1);
		assert.ok(data.panels[0].targets);
		assert.equal(data.panels[0].targets.length, 3);
		assert.equal(typeof data.content.type, 'function');
		assert.equal(data.content.$, 'It works! 1');
	});
});

describe('YML', () => {
	it('Should work', () => {
		const data = myLittleConfig(__dirname + '/data/4.yml');

		assert.ok(data);
		assert.ok(data.root);
		assert.equal(data.root.lol, 1);
		assert.equal(data.root.kek, 2);
		assert.equal(data.root.cheburek, 3);
	});
});
