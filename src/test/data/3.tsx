
export default args => (
	<dashboard name={args.name}>
		<panels type="array">
			<panel>
				<targets type="array">
					<target name="first"
							target={builder => builder.query(q => q.project('lol'))}/>
					<target name="second"
							target={builder => builder.query(q => q.project('kek'))}/>
					<target name="third"
							target={builder => builder.query(q => q.project('cheburek'))}/>
				</targets>
			</panel>
		</panels>
		<content type={builder => builder('a')}>
			{`It works! ${1}`}
		</content>
	</dashboard>
);
